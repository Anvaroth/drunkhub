"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Starts the app
 */
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var controllers_1 = require("./controllers");
require("./entities/db");
var cors = require('cors');
var app = express_1.default();
/**
 * Add CORS support to the app
 * @param cors() calling the cors method
 */
app.use(cors());
/**
 * Enable the app to use JSON
 * @param bodyParse.json() calling the bodyparser on json
 */
app.use(body_parser_1.default.json());
/**
 * Add all the routers
 * @param router the routers are collecter in this
 */
app.use(controllers_1.router);
/**
 * Start the app on sepcified or on the 3000 port
 */
app.listen(process.env.PORT || 3000, function () {
    console.log('Server started.');
});
module.exports = app;
