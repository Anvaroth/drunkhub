"use strict";
/**
 * Schema of a drink
 */
var mongooseDrink = require('mongoose');
var Schema = mongooseDrink.Schema;
/**
 * Data of a drink
 * @param name name of the drink (string), is required
 * @param description description of a drink (string), is required
 * @param ingredients ingredients of a drink ([string]), is required
 * @param timestamps creation, modification date/time of a drink
 */
var Drink = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    ingredients: { type: [String], required: true },
}, { timestamps: true });
module.exports = mongooseDrink.model('drinks', Drink);
