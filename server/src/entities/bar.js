"use strict";
/**
 * Schema of a bar
 */
var mongooseBar = require('mongoose');
var SchemaBar = mongooseBar.Schema;
/**
 * Data of a bar
 * @param name name of the bar (string), is required
 * @param address address of the bar (string), is required
 * @param location location of the bar (number,number) where number is float, is required
 * @param stock stock of the bar ([number]), array of drink ids, is required
 * @param timestamps creation, modification date/time of a bar
 */
var Bar = new SchemaBar({
    name: { type: String, required: true },
    address: { type: String, required: true },
    location: { x: { type: Number, required: true }, y: { type: Number, required: true } },
    stock: { type: [Number], required: true }
}, { timestamps: true });
module.exports = mongooseBar.model('bars', Bar);
