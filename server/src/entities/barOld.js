"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BarOld = void 0;
var core_1 = require("@mikro-orm/core");
var BarOld = /** @class */ (function () {
    function BarOld() {
    }
    __decorate([
        core_1.PrimaryKey(),
        __metadata("design:type", Number)
    ], BarOld.prototype, "id", void 0);
    __decorate([
        core_1.Property(),
        __metadata("design:type", String)
    ], BarOld.prototype, "name", void 0);
    __decorate([
        core_1.Property(),
        __metadata("design:type", String)
    ], BarOld.prototype, "address", void 0);
    __decorate([
        core_1.Property(),
        __metadata("design:type", Object)
    ], BarOld.prototype, "location", void 0);
    __decorate([
        core_1.Property(),
        __metadata("design:type", Array)
    ], BarOld.prototype, "stock", void 0);
    BarOld = __decorate([
        core_1.Entity()
    ], BarOld);
    return BarOld;
}());
exports.BarOld = BarOld;
//interface Drinks extends Array<Drink>{}
var testing = [
    { id: 1, name: 'Margarita', quantity: 3, ingredients: ['Lime juice', 'Cointreau', 'Tequila'], extras: ['Lime Wheel'] },
    { id: 2, name: 'Bloody Mary', quantity: 0, ingredients: ['Tomato juice', 'Lemon juice', 'Worcestershire sauce', 'Vodka', 'Celery salt', 'Pepper', 'Tabasco'], extras: ['Lemon Wedge', 'Celery Stalk'] },
    { id: 3, name: 'Caipirinha', quantity: 2, ingredients: ['Cachaca', 'Lime', 'Sugar'], extras: ['Lime wheel'] },
    { id: 4, name: 'Pina Colada', quantity: 11, ingredients: ['White rum', 'Coconut cream', 'Pineapple juice'], extras: ['Pineapple wedge', 'Maraschino cherry'] },
    { id: 5, name: 'Gin and Tonic', quantity: 5, ingredients: ['Gin', 'Tonic water'], extras: ['Lime wedge'] },
];
