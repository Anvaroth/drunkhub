import { Entity, PrimaryKey, Property } from "@mikro-orm/core";


@Entity()
export class BarOld {
    @PrimaryKey()
    id!: number;

    @Property()
    name!: string;

    @Property()
    address!: string;

    @Property()
    location!: { x:number, y:number };

    @Property()
    stock!: Drink[];
}

interface Drink {
    id: number;
    name: string;
    quantity: number;
    ingredients:string[];
    extras:string[];
}

//interface Drinks extends Array<Drink>{}

var testing: Drink[] = [
    { id: 1, name: 'Margarita', quantity: 3, ingredients: ['Lime juice', 'Cointreau', 'Tequila'], extras: ['Lime Wheel'] },
    { id: 2, name: 'Bloody Mary', quantity: 0, ingredients:['Tomato juice','Lemon juice', 'Worcestershire sauce', 'Vodka', 'Celery salt', 'Pepper', 'Tabasco'], extras:['Lemon Wedge', 'Celery Stalk'] },
    { id: 3, name: 'Caipirinha', quantity: 2, ingredients:['Cachaca', 'Lime', 'Sugar'], extras:['Lime wheel'] },
    { id: 4, name: 'Pina Colada', quantity: 11, ingredients:['White rum', 'Coconut cream', 'Pineapple juice'], extras:['Pineapple wedge', 'Maraschino cherry'] },
    { id: 5, name: 'Gin and Tonic', quantity: 5, ingredients:['Gin','Tonic water'], extras:['Lime wedge']},
];