import * as mongoose from 'mongoose';

const barSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    location: {
      type: { x: Number, y: Number },
      required: true,
    },
    stock: {
      type: [Number],
      required: true,
    }
  },
  { timestamps: true }
);

mongoose.model("Bar", barSchema);