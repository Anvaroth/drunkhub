/**
 * Starts the app
 */
import express from 'express';
import bodyParser from 'body-parser';
import { router } from './controllers';
import './entities/db';
const cors = require('cors');

const app = express();
/**
 * Add CORS support to the app
 * @param cors() calling the cors method
 */
app.use(cors())
/**
 * Enable the app to use JSON
 * @param bodyParse.json() calling the bodyparser on json
 */
app.use(bodyParser.json());
/**
 * Add all the routers
 * @param router the routers are collecter in this 
 */
app.use(router);

/**
 * Start the app on sepcified or on the 3000 port
 */
app.listen(process.env.PORT || 3000, () => {
    console.log('Server started.');
});

module.exports = app;