"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var mongooseTest = require('mongoose');
var BarModel = require('../../src/entities/bar');
/**
 * @param barData This contains the data for a bar that will be used in the tests.
 */
var barData = { name: 'Test Bar', address: 'TestAddress', location: { x: 66, y: 99 }, stock: [1, 2] };
var DrinkModel = require('../../src/entities/drink');
/**
 * @param drinkData This contains the data for a drink that will be used in the tests.
 */
var drinkData = { name: 'Test Drink', description: 'Test description.', ingredients: ["A little this.", "A little that."] };
var MONGODB_URITest = process.env.PROD_MONGODB || 'mongodb+srv://admin:admin@cluster0.uknxm.mongodb.net/DrunkHUB?retryWrites=true&w=majority';
/**
 * Testing the Bar Entity.
 */
describe('Bar Entity Test:', function () {
    /**
     * First it tries to connect to the database.
     */
    beforeAll(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, mongooseTest.connect(MONGODB_URITest, { useNewUrlParser: true, useCreateIndex: true }, function (err) {
                        if (err) {
                            console.error(err);
                            process.exit(0);
                        }
                    })];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * Inserts a bar with the test data from barData. Checks if the saved bar's parameters are equal to the ones in barData.
     */
    it('Created and saved a bar successfully.', function () { return __awaiter(void 0, void 0, void 0, function () {
        var validBar, savedBar;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    validBar = new BarModel(barData);
                    return [4 /*yield*/, validBar.save()];
                case 1:
                    savedBar = _a.sent();
                    expect(savedBar._id).toBeDefined();
                    expect(savedBar.name).toBe(barData.name);
                    expect(savedBar.address).toBe(barData.address);
                    return [4 /*yield*/, savedBar.remove()];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * Inserts a bar with the test data from barData. Expects not assigned parameters to be undefined, bar schemas don't have space fields.
     */
    it('Inserted a bar successfully, but the fields not in the schema are undefined.', function () { return __awaiter(void 0, void 0, void 0, function () {
        var barWithInvalidField, savedBarWithInvalidField;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    barWithInvalidField = new BarModel({ name: 'Test Bar', address: 'TestAddress', location: { x: 66, y: 99 }, stock: [1, 2] });
                    return [4 /*yield*/, barWithInvalidField.save()];
                case 1:
                    savedBarWithInvalidField = _a.sent();
                    expect(savedBarWithInvalidField._id).toBeDefined();
                    expect(savedBarWithInvalidField.size).toBeUndefined();
                    return [4 /*yield*/, savedBarWithInvalidField.remove()];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * If a bar would be inserted without a required field, should throw an error.
     */
    it('Created a bar without a required field, got expected error.', function () { return __awaiter(void 0, void 0, void 0, function () {
        var barWithoutRequiredField, err, savedBarWithoutRequiredField, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    barWithoutRequiredField = new BarModel({ name: 'TekLoon' });
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, barWithoutRequiredField.save()];
                case 2:
                    savedBarWithoutRequiredField = _a.sent();
                    err = savedBarWithoutRequiredField;
                    return [3 /*break*/, 4];
                case 3:
                    e_1 = _a.sent();
                    err = e_1;
                    return [3 /*break*/, 4];
                case 4:
                    expect(err).toBeInstanceOf(mongooseTest.Error.ValidationError);
                    expect(err.errors.address).toBeDefined();
                    return [4 /*yield*/, barWithoutRequiredField.remove()];
                case 5:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * After the tests, close the connection with the database.
     */
    afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, mongooseTest.connection.close()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
});
/**
 * Testing the Drink Entity.
 */
describe('Drink Entity Test:', function () {
    /**
     * First it tries to connect to the database.
     */
    beforeAll(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, mongooseTest.connect(MONGODB_URITest, { useNewUrlParser: true, useCreateIndex: true }, function (err) {
                        if (err) {
                            console.error(err);
                            process.exit(0);
                        }
                    })];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * Inserts a drink with the test data from drinkData. Checks if the saved drink's parameters are equal to the ones in drinkData.
     */
    it('Created and saved a drink successfully.', function () { return __awaiter(void 0, void 0, void 0, function () {
        var validDrink, savedDrink;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    validDrink = new DrinkModel(drinkData);
                    return [4 /*yield*/, validDrink.save()];
                case 1:
                    savedDrink = _a.sent();
                    expect(savedDrink._id).toBeDefined();
                    expect(savedDrink.name).toBe(drinkData.name);
                    expect(savedDrink.description).toBe(drinkData.description);
                    return [4 /*yield*/, savedDrink.remove()];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * Inserts a drink with the test data from drinkData. Expects not assigned parameters to be undefined, drink schemas don't have flavor fields.
     */
    it('Inserted a drink successfully, but the fields not in the schema are undefined.', function () { return __awaiter(void 0, void 0, void 0, function () {
        var drinkWithInvalidField, savedDrinkWithInvalidField;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    drinkWithInvalidField = new DrinkModel({ name: 'Test Drink', flavor: "Bad.", description: 'Test description.', ingredients: ["A little this.", "A little that."], });
                    return [4 /*yield*/, drinkWithInvalidField.save()];
                case 1:
                    savedDrinkWithInvalidField = _a.sent();
                    expect(savedDrinkWithInvalidField._id).toBeDefined();
                    expect(savedDrinkWithInvalidField.flavor).toBeUndefined();
                    return [4 /*yield*/, savedDrinkWithInvalidField.remove()];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * If a drink would be inserted without a required field, should throw an error.
     */
    it('Created a drink without a required field, got expected error.', function () { return __awaiter(void 0, void 0, void 0, function () {
        var drinkWithoutRequiredField, err, savedDrinkWithoutRequiredField, e_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    drinkWithoutRequiredField = new DrinkModel({ name: 'The BEST Drink' });
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, drinkWithoutRequiredField.save()];
                case 2:
                    savedDrinkWithoutRequiredField = _a.sent();
                    err = savedDrinkWithoutRequiredField;
                    return [3 /*break*/, 4];
                case 3:
                    e_2 = _a.sent();
                    err = e_2;
                    return [3 /*break*/, 4];
                case 4:
                    expect(err).toBeInstanceOf(mongooseTest.Error.ValidationError);
                    expect(err.errors.description).toBeDefined();
                    return [4 /*yield*/, drinkWithoutRequiredField.remove()];
                case 5:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    /**
     * After the tests, close the connection with the database.
     */
    afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, mongooseTest.connection.close()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
});
