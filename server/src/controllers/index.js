"use strict";
/**
 * Collects the routers
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = require("express");
var bar_controller_1 = require("./bar.controller");
var drink_controller_1 = require("./drink.controller");
exports.router = express_1.Router();
/**
 * Adds the `bars` and `drinks` routers to the main router
 */
exports.router.use('/bars', bar_controller_1.barRouter);
exports.router.use('/drinks', drink_controller_1.drinkRouter);
