/**
 * Creates endpoints to the drinks
 */

import * as mongoose from 'mongoose';
import '../entities/db';
import { Router } from 'express';

export const drinkRouter = Router();

const Drink = mongoose.model("Drink");


drinkRouter
  /**
   * Create and save a drink
   */
  .post('', (req, res) => {
    /**
     * Create drink
     * @param name name of the drink
     * @param description short description of the drink
     * @param ingredients array of strings of ingredients
     */
    const drink = new Drink({
      name: req.body.name,
      description: req.body.description,
      ingredients: req.body.ingredients,
    });

    /**
     * Save drink
     */
    drink
      .save()
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occured while creating the drink.",
        });
      });
  })

  /**
   * Find all the drinks
   */
  .get('', (req, res) => {
  Drink.find()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occured while retrieving drinks.",
      });
    });
  })
  /**
   * Find drinks by id
   */
  .get('/:id', (req, res) => {
  const id = req.params.id;

  Drink.findById(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({ message: "Not found drink with id " + id });
      } else {
        res.send(data);
      }
    })
    .catch(() => {
      res
        .status(500)
        .send({ message: "Error retrieving drink with id=" + id });
    });
  })

  /**
   * Update drinks by id
   */
  .put('/:id', (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  Drink.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update drink with id=${id}. Maybe drink was not found!`,
        });
      } else res.send({ message: "drink was updated succesfully." });
    })
    .catch(() => {
      res.status(500).send({
        message: "Error updating drink with id=" + id,
      });
    });
  })

  /**
   * Delet drinks by id
   */
  .delete('/:id', (req, res) => {
  const id = req.params.id;

  Drink.findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete drink with id=${id}. Maybe drink was not found!`,
        });
      } else {
        res.send({
          message: "Drink was deleted successfully!",
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: "Could not delete drink with id=" + id,
      });
    });
  })