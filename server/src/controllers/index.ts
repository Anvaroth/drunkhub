/**
 * Collects the routers
 */

import { Router } from 'express';
import { barRouter } from './bar.controller';
import { drinkRouter } from './drink.controller';

export const router = Router();
/**
 * Adds the `bars` and `drinks` routers to the main router
 */
router.use('/bars', barRouter);
router.use('/drinks', drinkRouter);