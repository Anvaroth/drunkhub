"use strict";
/**
 * Creates endpoints to the drinks
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.drinkRouter = void 0;
var mongoose = __importStar(require("mongoose"));
require("../entities/db");
var express_1 = require("express");
exports.drinkRouter = express_1.Router();
var Drink = mongoose.model("Drink");
exports.drinkRouter
    /**
     * Create and save a drink
     */
    .post('', function (req, res) {
    /**
     * Create drink
     * @param name name of the drink
     * @param description short description of the drink
     * @param ingredients array of strings of ingredients
     */
    var drink = new Drink({
        name: req.body.name,
        description: req.body.description,
        ingredients: req.body.ingredients,
    });
    /**
     * Save drink
     */
    drink
        .save()
        .then(function (data) {
        res.send(data);
    })
        .catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occured while creating the drink.",
        });
    });
})
    /**
     * Find all the drinks
     */
    .get('', function (req, res) {
    Drink.find()
        .then(function (data) {
        res.send(data);
    })
        .catch(function (err) {
        res.status(500).send({
            message: err.message || "Some error occured while retrieving drinks.",
        });
    });
})
    /**
     * Find drinks by id
     */
    .get('/:id', function (req, res) {
    var id = req.params.id;
    Drink.findById(id)
        .then(function (data) {
        if (!data) {
            res.status(404).send({ message: "Not found drink with id " + id });
        }
        else {
            res.send(data);
        }
    })
        .catch(function () {
        res
            .status(500)
            .send({ message: "Error retrieving drink with id=" + id });
    });
})
    /**
     * Update drinks by id
     */
    .put('/:id', function (req, res) {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!",
        });
    }
    var id = req.params.id;
    Drink.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(function (data) {
        if (!data) {
            res.status(404).send({
                message: "Cannot update drink with id=" + id + ". Maybe drink was not found!",
            });
        }
        else
            res.send({ message: "drink was updated succesfully." });
    })
        .catch(function () {
        res.status(500).send({
            message: "Error updating drink with id=" + id,
        });
    });
})
    /**
     * Delet drinks by id
     */
    .delete('/:id', function (req, res) {
    var id = req.params.id;
    Drink.findByIdAndRemove(id)
        .then(function (data) {
        if (!data) {
            res.status(404).send({
                message: "Cannot delete drink with id=" + id + ". Maybe drink was not found!",
            });
        }
        else {
            res.send({
                message: "Drink was deleted successfully!",
            });
        }
    })
        .catch(function () {
        res.status(500).send({
            message: "Could not delete drink with id=" + id,
        });
    });
});
