import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DrinkService } from 'src/app/service/drink.service';
import { Cocktail } from 'src/app/_models/Cocktail.Model';

@Component({
  selector: 'app-drink-profile',
  templateUrl: './drink-profile.component.html',
  styleUrls: ['./drink-profile.component.scss']
})
export class DrinkProfileComponent implements OnInit, OnDestroy {
  cocktailDetailSub!: Subscription;
  singleCocktail!: Cocktail;

  constructor(private cocktailSvc: DrinkService) {}

  ngOnInit(): void{
    this.cocktailDetailSub = this.cocktailSvc.cocktailSubject.subscribe(res => {
      this.singleCocktail = res;
      console.log(this.singleCocktail);
    });
  }

  ngOnDestroy(): void {
    this.cocktailDetailSub.unsubscribe();
  }
}
