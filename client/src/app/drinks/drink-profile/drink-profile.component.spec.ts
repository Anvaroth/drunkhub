import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkProfileComponent } from './drink-profile.component';

describe('DrinkProfileComponent', () => {
  let component: DrinkProfileComponent;
  let fixture: ComponentFixture<DrinkProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrinkProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /* it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
