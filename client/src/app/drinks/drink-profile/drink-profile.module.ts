import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DrinkProfileComponent } from './drink-profile.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: DrinkProfileComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DrinkProfileComponent]
})
export class DrinkProfileModule { }
