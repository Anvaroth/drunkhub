import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DrinkService } from '../service/drink.service';
import { DrinkProfileComponent } from './drink-profile/drink-profile.component';


@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.scss']
})
export class DrinksComponent implements OnInit {
  cocktailList: any[] | undefined;

  constructor(private cocktailSvc: DrinkService, private modalService: NgbModal) {}

  ngOnInit(): void {
    this.cocktailSvc.getAlcoholicCocktails().subscribe(res => {
      this.cocktailList = res;
      console.log(this.cocktailList);
    });
  }

  cocktailDetail(id: string): void {
    console.log(id);
    this.cocktailSvc.getAlcoholicCocktail(id);
    this.modalService.open(DrinkProfileComponent, {windowClass: 'md', centered: true});
  }
}
