import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DrinksComponent } from './drinks.component';

const routes: Routes = [
  {
    path: '',
    component: DrinksComponent,
    children: [
      {
        path: 'drink-profile',
        loadChildren: () =>
          import('./drink-profile/drink-profile.module').then(
            (m) => m.DrinkProfileModule
          ),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class DrinksModule {}
