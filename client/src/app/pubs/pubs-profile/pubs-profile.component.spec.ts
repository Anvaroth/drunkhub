import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PubsProfileComponent } from './pubs-profile.component';

describe('PubsProfileComponent', () => {
  let component: PubsProfileComponent;
  let fixture: ComponentFixture<PubsProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PubsProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PubsProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
/*
  it('should create', () => {
    expect(component).toBeTruthy();
  });
*/
});
