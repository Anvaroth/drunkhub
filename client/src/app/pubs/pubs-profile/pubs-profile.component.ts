import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { PubTransferService } from 'src/app/service/pub-transfer.service';
import { Pub, Location } from 'src/app/_models/models';

@Component({
  selector: 'app-pubs-profile',
  templateUrl: './pubs-profile.component.html',
  styleUrls: ['./pubs-profile.component.scss'],
})
export class PubsProfileComponent implements OnInit {
  selected: any[] | undefined;
  selectedLocation: Location = { x: 0, y: 0 };

  constructor(
    public service: PubTransferService,
    private router: Router,
    private modalService: NgbModal,
    private http: HttpClient,
    public dataService: PubTransferService,
    configModal: NgbModalConfig,
  ) {
      configModal.backdrop = 'static';
      configModal.backdropClass = 'z-990';
      configModal.centered = true;
      configModal.keyboard = false;
      configModal.windowClass = 'my-modal';
    }

  ngOnInit(): void {
    this.http
      .get(
        'https://nominatim.openstreetmap.org/search?q=Happy+Drink+Bar&format=json'
      )
      .subscribe((tmp: any) => {
        this.selected = tmp;
        console.log(this.selected);
    });
  }
  onCancel(): void {
    this.router.navigate(['/pubs']);
    this.modalService.dismissAll();
  }

  drinkNavigate(): void {
    this.router.navigateByUrl('/drinks');
  }

  locationNavigate(pub: Pub): void{
    this.selectedLocation = pub.location;
    this.router.navigateByUrl('locations');
  }
}
