import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PubsProfileComponent } from './pubs-profile.component';

const routes: Routes = [
  {
    path: '',
    component: PubsProfileComponent,
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class PubsProfileModule {}
