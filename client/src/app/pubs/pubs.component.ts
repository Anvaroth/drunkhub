import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BarService } from '../service/bar.service';
import { PubTransferService } from '../service/pub-transfer.service';
import { PubsProfileComponent } from './pubs-profile/pubs-profile.component';

@Component({
  selector: 'app-pubs',
  templateUrl: './pubs.component.html',
  styleUrls: ['./pubs.component.scss'],
})
export class PubsComponent implements OnInit, OnDestroy {
  constructor(
    public dataService: PubTransferService,
    private barService: BarService,
    private http: HttpClient,
    private modalService: NgbModal,
  ) {}

  ngOnInit(): void {
    this.http
      .get(
        'https://nominatim.openstreetmap.org/search?q=Happy+Drink+Bar&format=json'
      )
      .subscribe((tmp: any) => {
        console.log('Happy', tmp);
      });
  }

  OpenModal(id: string): void {
    this.barService.getBarById(id);
    console.log(id);
    this.modalService.open(PubsProfileComponent, {windowClass: 'md', centered: true});
  }

  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }
}
