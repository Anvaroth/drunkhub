import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { PubsComponent } from './pubs.component';

describe('PubsComponent', () => {
  let component: PubsComponent;
  let fixture: ComponentFixture<PubsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PubsComponent],
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [Router, HttpClient],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
/*
  it('should create', () => {
    expect(component).toBeTruthy();
  });
*/
});
