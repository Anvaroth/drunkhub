import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PubsComponent } from './pubs.component';

const routes: Routes = [
  {
    path: '',
    component: PubsComponent,
    children: [
      {
        path: 'pubs-profile',
        loadChildren: () =>
          import('./pubs-profile/pubs-profile.module').then(
            (m) => m.PubsProfileModule
          ),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class PubsModule {}
