import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cocktail } from '../_models/Cocktail.Model';

@Injectable({
  providedIn: 'root'
})
export class DrinkService {
  cocktailSubject = new Subject<Cocktail>();

  constructor(private http: HttpClient) {}

  readonly endpoint: string = 'https://www.thecocktaildb.com/api/json/v1/1/';

  getAlcoholicCocktails(): Observable<Cocktail[]> {
    return this.http
      .get(this.endpoint + 'filter.php?c=Cocktail')
      .pipe(map((data: any) => data.drinks.map(Cocktail.adapt)));
  }

  getAlcoholicCocktail(id: string): void {
    this.http
      .get(this.endpoint + 'lookup.php?i=' + id)
      .pipe(map((data: any) => data.drinks.map(Cocktail.adapt)))
      .subscribe(res => {
        console.log(res);
        this.cocktailSubject.next(res[0]);
      });
  }
}
