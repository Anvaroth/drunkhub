import { Injectable } from '@angular/core';
import { Drink, Pub } from '../_models/models';
import { BarService } from './bar.service';
import { DrinkService } from './drink.service';

@Injectable({
  providedIn: 'root',
})
export class PubTransferService {
  sharedPubs: Pub[] = [];
  private sharedDrinks: Drink [] = [];

  constructor(private bar: BarService, private drink: DrinkService) {}

  loadFromDatabase(): void {
    this.bar.getBars().subscribe((bars: Pub[]) => {
      this.sharedPubs = bars;
    });
  }

  getPubs(): Pub[] {
    return this.sharedPubs;
  }

  addPub(pub: Pub): void {
    this.sharedPubs.push(pub);
  }

  removePub(pub: Pub): void {
    this.sharedPubs = this.sharedPubs.filter((p) => p.name !== pub.name);
  }

  getDrinks(): Drink[] {
    return this.sharedDrinks;
  }

  addDrink(drink: Drink): void {
    this.sharedDrinks.push(drink);
  }

  removeDrink(drink: Drink): void {
    this.sharedDrinks = this.sharedDrinks.filter((p) => p.name !== drink.name);
  }
}
