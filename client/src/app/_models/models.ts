export interface Pub {
  id: string;
  image_url?: string;
  name: string;
  address?: string;
  location: Location;
  stock?: number[];
  createdAt?: string;
  updatedAt?: string;
}

export interface Location {
  x: number;
  y: number;
}

export interface Drink {
  name: string;
  imgThumb: string;
  id: string;
}
