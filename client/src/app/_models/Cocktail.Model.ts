import { Drink } from './models';

export class Cocktail implements Drink {
  name: string;
  imgThumb: string;
  id: string;
  instructions?: string;
  ingredient1?: string;
  ingredient2?: string;
  ingredient3?: string;
  ingredient4?: string;
  ingredient5?: string;
  ingredient6?: string;
  ingredient7?: string;
  ingredient8?: string;
  measure1?: string;
  measure2?: string;
  measure3?: string;
  measure4?: string;
  measure5?: string;
  measure6?: string;
  measure7?: string;
  measure8?: string;
  constructor(
    name: string,
    imgThumb: string,
    id: string,
    instructions?: string,
    ingredient1?: string,
    ingredient2?: string,
    ingredient3?: string,
    ingredient4?: string,
    ingredient5?: string,
    ingredient6?: string,
    ingredient7?: string,
    ingredient8?: string,
    measure1?: string,
    measure2?: string,
    measure3?: string,
    measure4?: string,
    measure5?: string,
    measure6?: string,
    measure7?: string,
    measure8?: string
  ) {
    this.name = name;
    this.imgThumb = imgThumb;
    this.id = id;
    this.instructions = instructions;
    this.ingredient1 = ingredient1;
    this.ingredient2 = ingredient2;
    this.ingredient3 = ingredient3;
    this.ingredient4 = ingredient4;
    this.ingredient5 = ingredient5;
    this.ingredient6 = ingredient6;
    this.ingredient7 = ingredient7;
    this.ingredient8 = ingredient8;
    this.measure1 = measure1;
    this.measure2 = measure2;
    this.measure3 = measure3;
    this.measure4 = measure4;
    this.measure5 = measure5;
    this.measure6 = measure6;
    this.measure7 = measure7;
    this.measure8 = measure8;
  }

  static adapt(item: any): Cocktail {
    return new Cocktail(
      item.strDrink,
      item.strDrinkThumb,
      item.idDrink,
      item.strInstructions,
      item.stringredient1,
      item.stringredient2,
      item.stringredient3,
      item.stringredient4,
      item.stringredient5,
      item.stringredient6,
      item.stringredient7,
      item.stringredient8,
      item.strMeasure1,
      item.strMeasure2,
      item.strMeasure3,
      item.strMeasure4,
      item.strMeasure5,
      item.strMeasure6,
      item.strMeasure7,
      item.strMeasure8,
    );
  }
}
