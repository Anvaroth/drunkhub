import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BarService } from './service/bar.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DrinksComponent } from './drinks/drinks.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocationsComponent } from './locations/locations.component';
import { MapComponent } from './map/map.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PubsComponent } from './pubs/pubs.component';
import { PubsProfileComponent } from './pubs/pubs-profile/pubs-profile.component';
import { PubTransferService } from './service/pub-transfer.service';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { DrinkService } from './service/drink.service';

@NgModule({
  declarations: [
    AppComponent,
    DrinksComponent,
    LocationsComponent,
    MapComponent,
    PubsComponent,
    PubsProfileComponent,
    MenuComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    NgbModule,
    RouterModule.forRoot([]),
  ],
  providers: [PubTransferService, BarService, HttpClient, DrinkService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
