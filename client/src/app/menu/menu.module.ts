import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu.component';

const routes: Routes = [
  {
    path: '',
    component: MenuComponent,
    children: [
      {
        path: 'pubs',
        loadChildren: () =>
          import('../pubs/pubs.module').then((m) => m.PubsModule),
      },
      {
        path: 'drinks',
        loadChildren: () =>
          import('../drinks/drinks.module').then((m) => m.DrinksModule),
      },
      {
        path: 'locations',
        loadChildren: () =>
          import('../locations/locations.module').then(
            (m) => m.LocationsModule
          ),
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class MenuModule {}
