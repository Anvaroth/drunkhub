import { Component, OnInit } from '@angular/core';
import { PubTransferService } from '../service/pub-transfer.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  title = 'DrunkHUB';
  constructor(private dataService: PubTransferService) {}

  ngOnInit(): void {
    this.dataService.loadFromDatabase();
  }

}
