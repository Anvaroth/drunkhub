import { Component, OnInit } from '@angular/core';
import { PubTransferService } from './service/pub-transfer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  title = 'DrunkHUB';
  page = 1;
  constructor(private dataService: PubTransferService) {}

  ngOnInit(): void {
    this.dataService.loadFromDatabase();
  }
}
