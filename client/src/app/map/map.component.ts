import { OnChanges, SimpleChanges } from '@angular/core';
import { AfterViewInit, Component, Input } from '@angular/core';
import * as L from 'leaflet';
import { Location } from '../_models/models';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit, OnChanges {
  private map: any;

  @Input() location!: Location;

  constructor() {}

  ngAfterViewInit(): void {
    this.initMap();
    const tiles = L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }
    );
    tiles.addTo(this.map);
  }

  private initMap(): void {
    this.map = L.map('map', {
      center: [0, 0],
      zoom: 1,
    });
  }

  public changeLocation(location: Location): void {
    // TODO: Mindig csak 1 Marker legyen
    this.map.setView(new L.LatLng(location.y, location.x), 17);
    const iconRetinaUrl = 'assets/marker-icon-2x.png';
    const iconUrl = 'assets/marker-icon.png';
    const shadowUrl = 'assets/marker-shadow.png';
    const iconDefault = L.icon({
      iconRetinaUrl,
      iconUrl,
      shadowUrl,
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41],
    });

    L.Marker.prototype.options.icon = iconDefault;
    L.marker([location.y, location.x]).addTo(this.map);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const location = 'location';
    if (changes[location]) {
      if (this.map !== undefined) {
        this.changeLocation(this.location);
      }
    }
  }
}
